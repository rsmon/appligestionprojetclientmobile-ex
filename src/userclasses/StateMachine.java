package userclasses;

import com.codename1.ui.*; 
import com.codename1.ui.events.*;
import com.codename1.ui.list.ListModel;
import com.codename1.ui.table.Table;
import com.codename1.xml.Element;
import generated.StateMachineBase;
import java.util.ArrayList;
import java.util.Vector;
import userclasses.modele.ModeleDeTableau;
import static userclasses.parser.AidePourVisiterArbreXML.getElementDansPour;
import static userclasses.parser.AidePourVisiterArbreXML.getLesElementsFils;
import static userclasses.parser.AidePourVisiterArbreXML.getValeur;
import userclasses.parser.RequetePourArbreXML;

public class StateMachine extends StateMachineBase {
      
  private RequetePourArbreXML   requeteRestFonctions; 
  private Element               reponseRacineArbreXmlFonctions;
  private Vector<Element>       lesElementsFonction;
    
  private ComboBox              comboBox;
  private ListModel             modeleDuComboBox;
  private String                codeFonctionSel;
   private String               codeFonctionSelAvantRefresh=null;
  
  
  private Label                libelleFonction;
  
  private Table                 tableau;
  private ModeleDeTableau       modeleDuTableau; 
  private ArrayList<ArrayList>  donneesDuModeleDuTableau; 
  
  private TextField             champSommeSalairesFonctionSel;
  private Float                 sommeSalairesFonctionSel=0f;
  private Long                   effectifFonctionSel=0L;
   
  public  StateMachine(String resFile) {super(resFile);}
  
  @Override
  protected void postMain(Form f) {
         
        super.postMain(f);
        
        comboBox=findComboBox(); tableau=findTable(); champSommeSalairesFonctionSel=findTextField();
        
        modeleDuComboBox=comboBox.getModel();
       
        libelleFonction=this.findLabel();
        
        tableau.setVisible(false);
        tableau.setDrawBorder(true); tableau.setScrollableY(true); 
        donneesDuModeleDuTableau = new ArrayList<ArrayList>();
        modeleDuTableau          = new ModeleDeTableau(donneesDuModeleDuTableau);
        
        requeteRestFonctions     = new RequetePourArbreXML();  
       
        initialiserLesDonnees(false); 
  }

  private void initCombo() {
                    
    for (Element elementFonction: lesElementsFonction){ modeleDuComboBox.addItem(getValeur(elementFonction,"codefonct"));}  
    tableau.setVisible(true);
    
  }
  
  @Override
  protected void onMain_ComboBoxAction(Component c, ActionEvent event) { traiterSelectionCombo(false); }
       
  private void traiterSelectionCombo(boolean rechargeDonnees) {
      
       if(rechargeDonnees)comboBox.setSelectedItem(codeFonctionSelAvantRefresh);
     
       codeFonctionSel=(modeleDuComboBox.getItemAt(modeleDuComboBox.getSelectedIndex())).toString();
        
       Element          fonction            = getElementDansPour(lesElementsFonction,"codefonct",codeFonctionSel);                                   
       Vector<Element>  lesElementsSalarie  = getLesElementsFils(fonction,"lessalaries");
      
       libelleFonction.setText(getValeur(fonction,"libfonct"));
       this.findLabel1().setText(getValeur(fonction,"libfonct"));
       
       modeleDuTableau.remplirEntete("num�ro","nom","salaire");  
       sommeSalairesFonctionSel=0f;effectifFonctionSel=0L;
       
       for( Element eltSal : lesElementsSalarie){
                   
          modeleDuTableau.ajouterRangee(getValeur(eltSal,"numsal"),getValeur(eltSal,"nomsal"),getValeur(eltSal,"salaire")+" �");
          sommeSalairesFonctionSel+=Float.valueOf(getValeur(eltSal,"salaire"));
          effectifFonctionSel++;
       }
       tableau.setModel(modeleDuTableau);  
       
       champSommeSalairesFonctionSel.setText(sommeSalairesFonctionSel.toString()+" �");  
       findTextField1().setText(effectifFonctionSel.toString());
  }

    @Override
    protected void onMain_ButtonAction(Component c, ActionEvent event) {
        
        this.codeFonctionSelAvantRefresh=this.codeFonctionSel;
        while (comboBox.getModel().getSize()>0) {comboBox.getModel().removeItem(0);}
        initialiserLesDonnees(true);        
    }

    private void initialiserLesDonnees(boolean rechargeDonnees) {
        
        requeteRestFonctions.executer("/entites/touteslesfonctions/");          
        reponseRacineArbreXmlFonctions = requeteRestFonctions.getRacine();
        lesElementsFonction            = getLesElementsFils(reponseRacineArbreXmlFonctions,"fonction");
        System.out.println(reponseRacineArbreXmlFonctions); 
        initCombo();    
        traiterSelectionCombo(rechargeDonnees); 
    }

    
}
     