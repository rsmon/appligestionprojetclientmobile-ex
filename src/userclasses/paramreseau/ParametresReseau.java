
package userclasses.paramreseau;

/**
 *
 * @author rsmon
 */

public class ParametresReseau {
     
    private static String ADRESSESERVEUR      = "94.23.28.21";
    private static String PORT                = "8080";
    private static String APPLI               = "AppliGestionProjets-war";
    private static String ENTREE_SERVICE      = "rest";
    
    public  static String RACINE_SERVICE_REST = "http://"+ADRESSESERVEUR+":"+PORT+"/"+APPLI+"/"+ENTREE_SERVICE;
          
}
