
package userclasses.parser;

import com.codename1.io.Util;
import com.codename1.xml.Element;
import com.codename1.xml.XMLParser;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 *
 * @author rsmon
 */
public class AidePourParser {
  
    public static Element getArbreXml(InputStream input) throws IOException {
               
        Reader   rd      = construireLecteurDeFlux(input);
        
        Element  racine  = new XMLParser().parse(rd);
        return   racine;
    }

    
    
    public static String getString(InputStream input)throws IOException {
       
      
        Reader   rd     = construireLecteurDeFlux(input); 
        
        StringBuilder sb= new StringBuilder(); 
       
        int c=rd.read();
        while(c>0){
            sb.append((char)c);
            c=rd.read();
        }
        return    sb.toString();
    }
      
    private static Reader construireLecteurDeFlux(InputStream input) throws IOException {
                
        input     = new ByteArrayInputStream(Util.readInputStream(input));
        Reader rd = new InputStreamReader(input);
        return rd;
    }
   
}
