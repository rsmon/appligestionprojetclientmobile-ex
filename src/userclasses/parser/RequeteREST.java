
package userclasses.parser;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import static userclasses.paramreseau.ParametresReseau.RACINE_SERVICE_REST;

/**
 *
 * @author rsmon
 */
public class RequeteREST extends ConnectionRequest{
   
    private String racine_Service_Rest;

    public RequeteREST() {
        
        racine_Service_Rest= RACINE_SERVICE_REST;
        setPost(false);    
    }
   
    public void executer(String url ){
               
       setUrl(racine_Service_Rest+url);
       executer();   
    }
      
    public void executer( ){
    
       NetworkManager.getInstance().addToQueueAndWait(this);
    } 
        
    public void executer(String url,boolean attendre ){
   
       setUrl(racine_Service_Rest+url); 
       executer(attendre); 
    }
    
    public void executer(boolean attendre ){

       if (attendre)NetworkManager.getInstance().addToQueueAndWait(this);
       else NetworkManager.getInstance().addToQueue(this);
    } 
 
}
