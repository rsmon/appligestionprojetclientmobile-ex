
package userclasses.parser;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author rsmon
 */
public class RequetePourTexteBrut extends RequeteREST{
   
    private String       texte;

    public String getTexte() { return texte; }

    @Override
    protected void postResponse() { }  

    @Override
    protected void readResponse(InputStream input) throws IOException {
            
        texte = AidePourParser.getString(input);
    }
           
}
