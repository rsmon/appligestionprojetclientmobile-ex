
package userclasses.parser;

import com.codename1.xml.Element;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author rsmon
 */
public class RequetePourArbreXML extends RequeteREST// ConnectionRequest
{
   
    private Element      racine;
    
    public Element getRacine() {
        return racine;
    }
    
    @Override
    protected void postResponse() { }  

    @Override
    protected void readResponse(InputStream input) throws IOException {
            
              racine = AidePourParser.getArbreXml(input);        
    }

}
